import { Module } from '@nestjs/common';
import { ScoringGateway } from './scoring.gateway';

@Module({
  imports: [],
  controllers: [],
  providers: [ScoringGateway],
})
export class AppModule {}
