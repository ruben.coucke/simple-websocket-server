import {ConnectedSocket, MessageBody, OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit, SubscribeMessage, WebSocketGateway, WebSocketServer,} from '@nestjs/websockets';
import {Logger} from '@nestjs/common';
import * as WebSocket from 'ws';

@WebSocketGateway()
export class ScoringGateway implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit {
    @WebSocketServer() server;
    connections: number = 0;

    private logger: Logger = new Logger();

    handleConnection() {
        this.connections++;
        this.logger.log('new client connected. Count: ' + this.connections);
    }

    handleDisconnect() {
        this.connections--;
        this.logger.log('Client disconnected. Count: ' + this.connections);
    }

    @SubscribeMessage('msgToServer')
    handleMsgToServer(@ConnectedSocket() client, @MessageBody() data: any): void {
        this.server.clients.forEach(client => {
          if (client !== this.server && client.readyState === WebSocket.OPEN) {
            client.send(JSON.stringify(data));
          }
        })
    }

    afterInit(server: any): any {
        this.logger.log('Websocket server started!');
    }
}
